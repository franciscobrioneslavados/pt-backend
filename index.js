 const app = require('./app');

 /* istanbul ignore next */
 app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});