const UserService = require("../services/auth");
const JWT = require("../config/security/jwt");

/**
 * Register new user
 *
 * @param {*} req
 * @param {*} res
 */
const signupUser = async (req, res) => {
  const serviceResponse = {};
  try {
    const username = req.body.username;
    const password = req.body.password;
    if (username && password) {
      const userService = new UserService();
      const userSigned = await userService.register(username, password);
      if (userSigned) {
        serviceResponse.code = 201;
        (serviceResponse.message = "User created"),
          (serviceResponse.data = userSigned);
      } else {
        (serviceResponse.code = 403),
          (serviceResponse.message = "Can created on redis!");
      }
    } else {
      serviceResponse.code = 400;
      serviceResponse.message = "Username or password params error!";
    }
  } catch (exception) {
    serviceResponse.system_message = exception.toString();
    serviceResponse.code = 500;
    serviceResponse.message = "Exception error!";
  } finally {
    await res.status(serviceResponse.code).json(serviceResponse);
  }
};

/**
 * Login and validate user
 *
 * @param {*} req
 * @param {*} res
 */
const signinUser = async (req, res) => {
  const serviceResponse = {};
  try {
    const username = req.body.username;
    const password = req.body.password;
    if (username && password) {
      const userService = new UserService();
      const userCorrect = await userService.login(username, password);
      if (userCorrect) {
        const tokenUser = await JWT.generateToken(username);
        serviceResponse.message = "Login Success";
        serviceResponse.data = tokenUser;
        serviceResponse.code = 200;
      } else {
        serviceResponse.code = 401;
        serviceResponse.message =
          "the password does not correspond or the user does not exist";
      }
    } else {
      serviceResponse.code = 400;
      serviceResponse.message = "Request error!";
    }
  } catch (exception) {
    serviceResponse.code = 500;
    serviceResponse.message = "Exception error!";
    serviceResponse.system_message = exception.toString();
  } finally {
    await res.status(serviceResponse.code).json(serviceResponse);
  }
};

module.exports = {
  signinUser,
  signupUser,
};
