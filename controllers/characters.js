const characterServices = require('../services/character');
const JWT = require('../config/security/jwt');

const getCharaters = async (req, res) => {
    const serviceResponse = {};
      try {
        const token = req.headers['token'];
        const username = req.headers['username'];
        const page = req.query['page'];

        // console.info(`token: ${token}, username: ${username}, page: ${page}`);

        if (token && username) {
          await JWT.validateToken(token, username)
          const characterService = new characterServices();
          const characters = await characterService.getCharacters(page).catch(error => error);
          // console.info(characters);

          if (!characters.stack) {
            const tokenUser = await JWT.generateToken(username)
            console.info(tokenUser);

            serviceResponse.code = 200;
            serviceResponse.characters = characters;
          } else {
            serviceResponse.message = 'Error Request!';
            serviceResponse.code = 500;
          }
        } else {
          serviceResponse.message = 'User token Error!'
          serviceResponse.code = 400;
        }
      } catch (e) {
        serviceResponse.system_message = e.toString();
        serviceResponse.code = 500;
        serviceResponse.message = 'Exception error'
      } finally {
        await res.status(serviceResponse.code).json(serviceResponse);
      }
}


module.exports = {
    getCharaters
}