const express = require('express');
const router = express.Router();
const {getCharaters} = require('../controllers/characters');

router.get('/characters', getCharaters);

module.exports = router;