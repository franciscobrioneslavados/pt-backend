# Prueba Tecnica | Backend

## Endpoints

##### LOGIN | Method POST
```
http://localhost:8080/api/v1/auth/signin
```

##### REGISTER | Method POST
```
http://localhost:8080/api/v1/auth/signup
```


##### GET CHARACTERS | Method GET
```
http://localhost:8080/api/v1/characters?page={page}
```
