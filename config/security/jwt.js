const fs = require('fs');
const jwt = require('jsonwebtoken');
const privateKey = fs.readFileSync('./secrets/private.key');

class JWTSecurity {
  static generateToken(user) {
    return jwt.sign({user: user}, privateKey, {expiresIn: 120});
  }

  static async validateToken(token, user) {
    try {
      const data = await jwt.verify(token, privateKey);
      return data.user === user;
    } catch (exception) {
      throw new Error(exception);
    }
  }
}


module.exports = JWTSecurity;
