const redis = require('redis');
const {promisify} = require('util');
require('dotenv').config();
 /* istanbul ignore next */
const client = redis.createClient(process.env.REDIS_URL);


module.exports = {
  ...client,
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
  keysAsync: promisify(client.keys).bind(client)
};