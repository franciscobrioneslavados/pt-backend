const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cors = require('cors');

/** Setup */
const app = express();
const PORT = process.env.PORT || 8080;

/** Import Routes */
const authRoutes = require('./routes/auth');
const characterRoutes = require('./routes/character');

/** Middlewares */
app.use((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
})
app.use(cors());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(methodOverride());

/** Declare Routes */
app.use('/api/v1/auth', authRoutes);
app.use('/api/v1', characterRoutes);


module.exports = app