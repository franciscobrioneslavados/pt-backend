const request = require("supertest");
const jwt = require("jsonwebtoken");
const app = require("../../app");
const server = require('http').Server(app);
const agent = request.agent(server);
const redis = require("redis-mock");
const redisClient = redis.createClient();
const port = process.env.PORT || 3000;


describe("Testing Users Controllers |  LOGIN" , () => {
  server.listen(port, () => {
     /* istanbul ignore next */
    console.log('Servidor corriendo en puerto ' + port);
  });

  beforeEach(() => {
    jest.mock("request");
    jest.mock("jsonwebtoken");
    jest.mock("../../config/redis-client");
    jest.mock("../../services/auth");
  });

  afterAll(async () => {
    await new Promise(resolve => setTimeout(() => resolve(), 500)); 
    server.close();// avoid jest open handle error
  });

  test("params empty", async (done) => {
    const data = { username: "", password: "" };
    await agent.post("/api/v1/auth/signin").send(data).expect(400);
    done()
  });


  test("Testing user exist", async (done) => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('{"username": "tester", "password": "123456"}');

    const data = { username: "tester", password: "123456" };
    await agent.post("/api/v1/auth/signin").send(data).expect(200);
    done()
  });

  test("Testing user doesnt exist", async (done) => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue(false);

    const mockRedisClientSet = jest.fn();
    redisClient.setAsync = mockRedisClientSet;
    mockRedisClientSet.mockReturnValue(true);

    const data = { username: "tester", password: "123456" };
    await agent.post("/api/v1/auth/signin").send(data).expect(401);
    done();
  });
  
});

describe("Testing Users Controllers |  REGISTER" , () => {
  beforeEach(() => {
    jest.mock("request");
    jest.mock("jsonwebtoken");
    jest.mock("../../config/redis-client");
    jest.mock("../../services/auth");
  });



  test("Testing register new user Redis error", async (done) => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('{"username": "tester", password": "123456"}');

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue(false);

    const data = { user: "tester", password: "123456" };
    await agent.post("/api/v1/auth/signup").send(data).expect(400);

    done();
  });

  test("Testing register new user data empty", async (done) => {
    const mockRedisClientGet = jest.fn();
    redisClient.getAsync = mockRedisClientGet;
    mockRedisClientGet.mockReturnValue('{"username": "tester", password": "123456"}');

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue("abcd.efgh.ijkl");


    const data = { user: "tester", password: "123456" };
    await agent.post("/api/v1/auth/signup").send(data).expect(400);
    done();
  });

});