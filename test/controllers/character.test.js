const request = require("supertest");
const jwt = require("jsonwebtoken");
const app = require("../../app");
const server = require('http').Server(app);
const agent = request.agent(server);
const port = process.env.PORT || 3000;

describe("Testing Characters Controller", () => {
  server.listen(port, () => {
     /* istanbul ignore next */
    console.log('Servidor corriendo en puerto ' + port);
  });

  beforeEach(() => {
    jest.mock("request");
    jest.mock("jsonwebtoken");
    jest.mock("../../config/redis-client");
    jest.mock("../../services/auth");
  });

  afterAll(async () => {
    await new Promise(resolve => setTimeout(() => resolve(), 500)); 
    server.close();// avoid jest open handle error
  });

  test("Testing get Characters logged user", async (done) => {
    const mockJwtVerify = jest.fn();
    jwt.verify = mockJwtVerify;
    mockJwtVerify.mockReturnValue({ user: "tester" });

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue("abcd.efgh.ijkl");

    await agent
      .get("/api/v1/characters")
      .set("token", "abc.def.ghi", "username", 'tester')
      .expect(400);
    done()
  });

  test("Testing get Characters no logged user", async (done) => {
    const mockJwtVerify = jest.fn();
    jwt.verify = mockJwtVerify;
    mockJwtVerify.mockReturnValue({ user: "tester" });

    const mockJwtSign = jest.fn();
    jwt.sign = mockJwtSign;
    mockJwtSign.mockReturnValue("abcd.efgh.ijkl");

    await agent
      .get("/api/v1/characters")
      .set("token", "abc.def.ghi", "username", 'tester')
      .expect(400);
    done()
  });
});
